from flask_bcrypt import check_password_hash
from flask_restful import Resource, reqparse
from models.user import UserModel
from werkzeug.security import safe_str_cmp
from flask_jwt_extended import create_access_token, jwt_required, get_raw_jwt
from blacklist import BLACKLIST


class UserLogin(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument("username", type=str, required=True,
                        help="This field cannot be blank")
    parser.add_argument("password", type=str, required=True,
                        help="This field cannot be blank")

    def post(self):
        data = self.parser.parse_args()
        user = UserModel.find_by_username(data["username"])

        if user and check_password_hash(user.password, data['password']):
            access_token = create_access_token(identity=user.id)

            return {"access_token": access_token}, 200

        return {"message": "Invalid credantials"}, 401


class UserLogout(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()["jti"]
        BLACKLIST.add(jti)
        return {"message": "Successfully logged out."}, 200
