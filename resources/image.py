import os
from db import db
from flask_restful import Resource, request
from werkzeug.utils import secure_filename
from flask_jwt_extended import jwt_required, get_jwt_claims

from models.image import ImageModel
from models.car import CarModel


UPLOAD_FOLDER = 'static'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


class Image(Resource):
    @jwt_required
    def post(self, car_id):
        claims = get_jwt_claims()
        if not claims["is_admin"]:
            return {"message": "Admin privilege required."}

        for file in request.files.getlist("image"):
            if file and allowed_file(file.filename):
                filename = secure_filename(f"{car_id}_" + file.filename)
                image = file.save(os.path.join(UPLOAD_FOLDER, filename))

                car_save = ImageModel(name=filename, car_id=car_id)

                db.session.add(car_save)
                db.session.commit()

        #         return {"message": "Image Uploaded"}, 201

        # return {"message": "Method not allowed"}, 405

    def get(self, car_id):
        images = []
        img = ImageModel.query.filter_by(car_id=car_id).all()

        if img:
            for x in img:
                images.append(
                    f"https://test-api-cars.herokuapp.com/static/{x.name}")
            return {"images": images}, 200

        return {"message": "Image not found"}, 404


class AllImages(Resource):
    def get(self):
        return {"images": list(map(lambda x: x.json(), ImageModel.query.all()))}


class ImageDelete(Resource):
    @jwt_required
    def delete(self, car_id, name):
        claims = get_jwt_claims()
        if not claims["is_admin"]:
            return {"message": "Admin privilege required."}

        car_image = ImageModel.query.filter_by(car_id=car_id).first()

        if car_image is not None:
            find_img = ImageModel.query.filter_by(name=name).first()
            os.remove(os.path.join(UPLOAD_FOLDER, find_img.name))
            db.session.delete(find_img)
            db.session.commit()
            return {"message": "Image deleted"}, 200

        return {"message": "Image not found"}, 400
