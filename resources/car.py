import sqlite3
import os
from db import db
from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required, get_jwt_claims

from models.car import CarModel
from models.image import ImageModel

_car_parser = reqparse.RequestParser()
_car_parser.add_argument("car_id", type=int, required=True,
                         help="This field cannot be blank")
_car_parser.add_argument("car_name", type=str, required=True,
                         help="This field cannot be blank")
_car_parser.add_argument("car_km", type=int, required=True,
                         help="This field cannot be blank")
_car_parser.add_argument("car_price", type=int,
                         required=True, help="This field cannot be blank")
_car_parser.add_argument("car_ez", type=str, required=True,
                         help="This field cannot be blank")
_car_parser.add_argument("car_ps", type=str, required=True,
                         help="This field cannot be blank")
_car_parser.add_argument("car_treibstoff", type=str,
                         required=True, help="This field cannot be blank")
_car_parser.add_argument("car_farbe", type=str,
                         required=True, help="This field cannot be blank")
_car_parser.add_argument("car_kategorie", type=str,
                         required=True, help="This field cannot be blank")
_car_parser.add_argument("car_aufbau", type=str,
                         required=True, help="This field cannot be blank")
_car_parser.add_argument("car_getriebe", type=str,
                         required=True, help="This field cannot be blank")
_car_parser.add_argument("car_antrieb", type=str,
                         required=True, help="This field cannot be blank")
_car_parser.add_argument("car_turen", type=int,
                         required=True, help="This field cannot be blank")
_car_parser.add_argument("car_sitze", type=int,
                         required=True, help="This field cannot be blank")
_car_parser.add_argument("car_beschreibung", type=str,
                         required=True, help="This field cannot be blank")


class Car(Resource):

    def get(self, car_id):
        car = CarModel.find_car_by_id(car_id)
        if car:
            return car.json()

    @jwt_required
    def post(self, car_id):
        claims = get_jwt_claims()
        if not claims["is_admin"]:
            return {"message": "Admin privilege required."}

        if CarModel.find_car_by_id(car_id):
            return {"message": f"Car with id {car_id} already exist"}, 400

        data = _car_parser.parse_args()
        car = CarModel(
            data["car_id"],
            data["car_name"],
            data["car_km"],
            data["car_price"],
            data["car_ez"],
            data["car_ps"],
            data["car_treibstoff"],
            data["car_farbe"],
            data["car_kategorie"],
            data["car_aufbau"],
            data["car_getriebe"],
            data["car_antrieb"],
            data["car_turen"],
            data["car_sitze"],
            data["car_beschreibung"]
        )

        car.save_to_db()

        return car.json(), 201

    @jwt_required
    def delete(self, car_id):
        claims = get_jwt_claims()
        if not claims["is_admin"]:
            return {"message": "Admin privilege required."}

        car = CarModel.find_car_by_id(car_id)

        if car:
            car_images = ImageModel.query.filter_by(car_id=car_id).all()
            car.delete_from_db()
            for x in car_images:
                os.remove(os.path.join("static", x.name))
                db.session.delete(x)
                db.session.commit()

        return {"message": "Car deleted"}, 200

    @jwt_required
    def put(self, car_id):
        claims = get_jwt_claims()
        if not claims["is_admin"]:
            return {"message": "Admin privilege required."}

        data = _car_parser.parse_args()

        car = CarModel.find_car_by_id(car_id)
        if car is None:
            car = CarModel(
                data["car_id"],
                data["car_name"],
                data["car_km"],
                data["car_price"],
                data["car_ez"],
                data["car_ps"],
                data["car_treibstoff"],
                data["car_farbe"],
                data["car_kategorie"],
                data["car_aufbau"],
                data["car_getriebe"],
                data["car_antrieb"],
                data["car_turen"],
                data["car_sitze"],
                data["car_beschreibung"]
            )
        else:
            car.car_name = data["car_name"]
            car.car_km = data["car_km"]
            car.car_price = data["car_price"]
            car.car_ez = data["car_ez"]
            car.car_ps = data["car_ps"]
            car.car_treibstoff = data["car_treibstoff"]
            car.car_farbe = data["car_farbe"]
            car.car_kategorie = data["car_kategorie"]
            car.car_aufbau = data["car_aufbau"]
            car.car_getriebe = data["car_getriebe"]
            car.car_antrieb = data["car_antrieb"]
            car.car_turen = data["car_turen"]
            car.car_sitze = data["car_sitze"]
            car.car_beschreibung = data["car_beschreibung"]

        car.save_to_db()

        return car.json()


class CarList(Resource):
    def get(self):
        return {"cars": list(map(lambda x: x.json(), CarModel.query.all()))}
