from db import db


class ImageModel(db.Model):
    __tablename__ = "images"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(1500))

    car_id = db.Column(db.Integer, db.ForeignKey('cars.car_id'))
    car = db.relationship('CarModel')

    def json(self):
        return {
            "id": self.id,
            "name": f"https://test-api-cars.herokuapp.com/static/{self.name}",
            "car_id": self.car_id
        }
