from db import db
from models.image import ImageModel


class CarModel(db.Model):

    __tablename__ = "cars"

    car_id = db.Column(db.Integer, primary_key=True)
    car_name = db.Column(db.String(20000))
    car_km = db.Column(db.Integer)
    car_price = db.Column(db.Integer)
    car_ez = db.Column(db.String(180))
    car_ps = db.Column(db.String(180))
    car_treibstoff = db.Column(db.String(180))
    car_farbe = db.Column(db.String(180))
    car_kategorie = db.Column(db.String(180))
    car_aufbau = db.Column(db.String(180))
    car_getriebe = db.Column(db.String(180))
    car_antrieb = db.Column(db.String(180))
    car_turen = db.Column(db.Integer)
    car_sitze = db.Column(db.Integer)
    car_beschreibung = db.Column(db.String(10000000))

    image = db.relationship("ImageModel")

    def __init__(self, car_id, car_name, car_km, car_price, car_ez, car_ps, car_treibstoff, car_farbe, car_kategorie, car_aufbau, car_getriebe, car_antrieb, car_turen, car_sitze, car_beschreibung):
        self.car_id = car_id
        self.car_name = car_name
        self.car_km = car_km
        self.car_price = car_price
        self.car_ez = car_ez
        self.car_ps = car_ps
        self.car_treibstoff = car_treibstoff
        self.car_farbe = car_farbe
        self.car_kategorie = car_kategorie
        self.car_aufbau = car_aufbau
        self.car_getriebe = car_getriebe
        self.car_antrieb = car_antrieb
        self.car_turen = car_turen
        self.car_sitze = car_sitze
        self.car_beschreibung = car_beschreibung

    def get_car_image(self, car_id):
        images = []
        img = ImageModel.query.filter_by(car_id=car_id).all()

        if img:
            for x in img:
                images.append(
                    f"https://test-api-cars.herokuapp.com/static/{x.name}")
            return images

        return {"message": "Image not found"}

    def json(self):
        return {
            "car_id": self.car_id,
            "car_name": self.car_name,
            "car_km": self.car_km,
            "car_price": self.car_price,
            "car_ez": self.car_ez,
            "car_ps": self.car_ps,
            "car_treibstoff": self.car_treibstoff,
            "car_farbe": self.car_farbe,
            "car_kategorie": self.car_kategorie,
            "car_aufbau": self.car_aufbau,
            "car_getriebe": self.car_getriebe,
            "car_antrieb": self.car_antrieb,
            "car_turen": self.car_turen,
            "car_sitze": self.car_sitze,
            "car_beschreibung": self.car_beschreibung,
            "car_image": self.get_car_image(self.car_id)
        }

    @classmethod
    def find_car_by_id(cls, car_id):
        return cls.query.filter_by(car_id=car_id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
